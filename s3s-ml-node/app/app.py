import os
from flask import Flask
from flask import request, redirect, url_for, flash, jsonify
from dotenv import load_dotenv 
import controllers.machinel as machinel
import routes.register_routes as register_routes
from flask_cors import CORS, cross_origin

 



def create_app(test_config=None):
    # Load env
    load_dotenv()

    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    cors = CORS(app)
    app.config['CORS_HEADERS'] = 'Content-Type'
    app.config.from_mapping(
        SECRET_KEY=os.getenv("SECRET", default = None)
    )


    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)


    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass


    #ensure the machine learning model
    machinel.setup()


    #register all routes
    app = register_routes.mregister_routes(app)

    #mode of running the server
    mode = os.getenv("MODE", default = "python")
    if mode == "python":
        print("python")
        return app
    else: 
        print("flask")   
        app.run(port=os.getenv("PORT", default = 3005), debug=os.getenv("DEBUG", default = bool(0)))


app=create_app()
        
