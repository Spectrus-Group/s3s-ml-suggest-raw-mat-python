# Dependencies
import numpy as np
import pickle as p
import json
import joblib
import traceback
import pandas as pd
import os
import templates.responce as responce
#from ..templates import responce





def setup():
    global model
    global model_columns
    global biocompounds
    try:
        modelfile = os.path.abspath(os.getcwd())+os.getenv("MODEL_PATH", default = None)
        model_columns = os.path.abspath(os.getcwd())+os.getenv("MODEL_COLUMN_PATH", default = None)
        model_data = os.path.abspath(os.getcwd())+os.getenv("MODEL_DATA_PATH", default = None)

        model = joblib.load(open(modelfile, 'rb'))
        model_columns = joblib.load(model_columns) # Load "model_columns.pkl"
        biocompounds = open(model_data, 'rt').read()
        print ('Model columns loaded -- model setup successfull')
    except Exception as e:
        print("Error in setting up ml model--exiting",os.path.abspath(os.getcwd())+os.getenv("MODEL_PATH", default = None))   
        print(e)
        exit("Error in setting up ml model--exiting") 
    return model

def predict(data):
    if model:
        try:
            print(data)
            prediction = np.array2string(model.predict(data))
            #prediction = model.predict(data)
            return responce.sresponce(bool(1),prediction)

        except:
            return responce.sresponce(bool(0),{'trace': traceback.format_exc()})
    else:
        return responce.sresponce(bool(0),{'error': 'No model here to use'})


def mbiocompounds():
    if biocompounds:
        try:
            print(biocompounds)
            return responce.sresponce(bool(1),biocompounds)

        except:
            return responce.sresponce(bool(0),{'trace': traceback.format_exc()})
    else:
        return responce.sresponce(bool(0),{'error': 'No biocompounds data'})

        
