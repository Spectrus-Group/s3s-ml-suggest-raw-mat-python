#!/bin/bash
export FLASK_APP=app
export FLASK_ENV=production
source activate py37
export PYTHONPATH=/home/ubuntu/anaconda2/envs/py37/site-packages
python --version
gunicorn app:app --daemon  -b localhost:3010 &

