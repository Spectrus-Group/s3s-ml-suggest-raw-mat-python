module.exports = {
  apps : [{
    name: 'API',
    script: './start-prod.sh',
    autorestart: true,
  }],
};
