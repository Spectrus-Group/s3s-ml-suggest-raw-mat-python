from flask import Blueprint
import templates.responce as responce
from flask_cors import CORS, cross_origin


ping = Blueprint('ping',__name__)
@ping.route("/ping")
@cross_origin()
def hello():
    return responce.sresponce(bool(1),"pong")


