from flask import Flask
import routes.predict as mpredict
import routes.ping as ping
import routes.biocompounds as biocompounds

def mregister_routes(app):
    app.register_blueprint(mpredict.predict)
    app.register_blueprint(ping.ping)
    app.register_blueprint(biocompounds.biocompounds)
    return app

