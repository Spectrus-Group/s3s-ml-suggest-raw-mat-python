import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PredictRoutingModule } from './predict-routing.module';
import { PredictComponent } from './predict.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [PredictComponent],
  imports: [
    CommonModule,
    PredictRoutingModule,
    SharedModule
  ]
})
export class PredictModule { }
