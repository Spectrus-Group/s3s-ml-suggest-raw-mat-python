import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-predict',
  templateUrl: './predict.component.html',
  styleUrls: ['./predict.component.scss']
})
export class PredictComponent implements OnInit {
  question_array: any = [
    {
      "key": "decompose",
      "question": `Our BioPur biocomposites are suitable for products that need to breakdown naturally after use. Which of these are your preferred condition to degrade?`,
      "ans": ["Home compostable", "Industry compostable", "Recycleble"]
    },
    {
      "key": "process",
      "question": `How do you intend to manufacture the product`,
      "ans": ["Blow film", "Blow moulding", "Injection blow moulding", "Injection moulding", "Thermoforming"]
    },
    {
      "key": "feel",
      "question": `Our biocomposites are free from toxic chemicals and they are suitable for food contact products, toys and games.
     Which of these best describes your product?`,
      "ans": ["Flexible", "High impact", "Rigid", "Semi flexible"]
    },
    {
      "key": "thickness",
      "question": `Average wall thickness of your product is ?`,
      "ans": ["Thickness<100micros", "Thickness<1mm", "Thickness<2mm", "Thickness<3mm", "Thickness<4mm", "Thickness<50microns", "Thickness<500micros", "Thickness>4mm"]
    },
    {
      "key": "safety_certs",
      "question": `Choose safety standards`,
      "ans": ["Dishwasher safe", "Food contact safe", "Microwave safe", "None", "Toy Safe"]
    },
    {
      "key": "fiber_visibility",
      "question": `How do you like the fiber visbility on the product?`,
      "ans": [
        "Black glossy, fiber not visible", "Black matt, fiber not visible",
        "Bright glossy, fine visible fibers", "Bright glossy, large visbible fibers", "Bright glossy, moderately visbible fibers",
        "Bright matt, fine visible fibers", "Bright matt, large visbible fibers", "Bright matt, moderately visbible fibers",
        "Dark glossy, fiber not visible", "Dark glossy, Large visible fibers", "Dark glossy, Moderate visible fibers",
        "Dark matt, fibers not visible", "Dark matt, large visbible fibers", "Dark matt, moderately visbible fibers",
        "None"]
    },
    {
      "key": "fiber_pref",
      "question": `Choose fiber preference`,
      "ans": ["Bamboo", "None"]
    },
    {
      "key": "compare_mat",
      "question": `We recommend BioDur, our durable and recyclable biocomposites for your industry. Which of these materials would you like to replace with biocomposites?`,
      "ans": ["ABS", "CPP", "GPPS", "HIPS", "HPP", "None", "PC", "PE", "PLA", "PPGF-10", "PPGF-20", "PPGF-30", "PPT-10", "PPT-20", "PPT-30"]
    }
  ]


  raw_material_map = {
    5: "BioDur PB10SS",
    11: "BioDur PB20SS",
    17: "BioDur PB30SS",
    23: "BioDur PB40SS",
    29: "BioDur PB50SS",
    3: "BioDur PB10MS",
    9: "BioDur PB20MS",
    15: "BioDur PB30MS",
    21: "BioDur PB40MS",
    27: "BioDur PB50MS",
    1: "BioDur PB10LS",
    7: "BioDur PB20LS",
    13: "BioDur PB30LS",
    19: "BioDur PB40LS",
    25: "BioDur PB50LS",
    4: "BioDur PB10SI",
    10: "BioDur PB20SI",
    16: "BioDur PB30SI",
    22: "BioDur PB40SI",
    28: "BioDur PB50SI",
    2: "BioDur PB10MI",
    8: "BioDur PB20MI",
    14: "BioDur PB30MI",
    20: "BioDur PB40MI",
    26: "BioDur PB50MI",
    0: "BioDur PB10LI",
    6: "BioDur PB20LI",
    12: "BioDur PB30LI",
    18: "BioDur PB40LI",
    24: "BioDur PB50LI",
    30: "BioPur B5120",
    31: "BioPur B5121",
    35: "BioPur B7011",
    36: "BioPur B7012",
    32: "BioPur B6040",
    33: "BioPur B6400",
    34: "BioPur B6500",
    38: "BioPur M7002",
    39: "BioPur M7003",
    40: "BioPur M7102",
    41: "BioPur M7103",
    37: "BioPur M6004",
    42: "BioPur N5002",
    43: "BioPur N5003",
    44: "BioPur N5004",
    45: "BioPur N5005",

  }

  show_result : boolean = false
  mapped_result  = "none"

  public form: FormGroup;
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
    private apiSerice: ApiService,private toast : ToastrService,public router : Router) { }

  ngOnInit() {
    this.init()
  }

  async init(){
    this.spinner.show()
    let questioneere = await this.apiSerice.getQuestioneere();
    this.spinner.hide()
    console.log("The quesioneere is ",questioneere)

    if(questioneere && questioneere.data && questioneere.data.Items){
      let temp = this.fb.group({
        questions :  this.fb.array([]),
  
        // decompose: [null, Validators.compose([Validators.required])],
        // process: [null, Validators.compose([Validators.required])],
        // feel: [null, Validators.compose([Validators.required])],
        // thickness: [null, Validators.compose([Validators.required])],
        // safety_certs: [null, Validators.compose([Validators.required])],
        // fiber_visibility: [null, Validators.compose([Validators.required])],
        // fiber_pref: [null, Validators.compose([Validators.required])],
        // compare_mat: [null, Validators.compose([Validators.required])],
      });

      let i=0;
      this.question_array = []
      //sort questions
      questioneere.data.Items.sort((a,b)=>{
        if(Number(a.priority)>Number(b.priority)){
          return +1
        }else{
          return -1;
        }
      })
      questioneere.data.Items.forEach(question => {
        let control = <FormArray>temp.controls.questions;
        let fc_name = "q"+ String(i)
        let temp_fg = new FormGroup({}) 
        temp_fg.addControl(fc_name,new FormControl(""));
        control.push(temp_fg);
        i++

        let question_array : any={}
        question_array.key = fc_name;
        question_array.question = question.question;
        let temp_answers = []
        //sort answers
        question.answers.sort((a,b)=>{
          if(Number(a.priority)>Number(b.priority)){
            return +1
          }else{
            return -1;
          }
        })

        question.answers.forEach(element => {
          temp_answers.push(element.answer)
        });
        question_array.ans = temp_answers;
        this.question_array.push(question_array)
      });

      this.form = temp;
    }

 

  }

  async again(){
    this.show_result = false;
    this.mapped_result  = "none"
  }

  async home(){
    this.router.navigate([""])
  }

  async onSubmit() {
    console.log(this.form.value)
    let binary_array = [];
    let binary_array_readable = []; 

    //convert to array to obj
    let obj_array_inputs = {};
    this.question_array.forEach(element => {
      obj_array_inputs[element.key] = element.ans
    });

    console.log("Array to OBJ", obj_array_inputs)

    let modified_form = {value:{}}

    this.form.value.questions.forEach(element => {
      let keys = Object.keys(element)
      modified_form.value[keys[0]] = element[keys[0]]
    });

    console.log("modiefied form ",modified_form)
    let form_keys = Object.keys(modified_form.value)


    console.log("Form keys ",form_keys)


  

    form_keys.forEach((key) => {
      let key_values = modified_form.value[key];
      obj_array_inputs[key].forEach(option => {

        let isFoundOption = key_values.find((keyvalue) => {
          if (keyvalue == option) {
            return true
          } else {
            return false
          }
        })
        if (isFoundOption) {
          binary_array_readable.push({ "option": option, "selected": 1 })
          binary_array.push(1)
        } else {
          binary_array_readable.push({ "option": option, "selected": 0 })
          binary_array.push(0)
        }

      });
    })

    // binary selection array
    console.log(" binary selection array readable", binary_array_readable)
    console.log(" binary selection array", binary_array)

    //hit the server
    this.spinner.show();
    let resp = await this.apiSerice.predict([binary_array]);
    console.log("Prediction responce is ", resp)
    if(resp.success==true){
      let raw_resp = resp.data
      if(raw_resp){
        let raw_material_number = raw_resp.split(" ")
        console.log(raw_material_number)
        let temp_raw_resp = raw_material_number[0].replace("[[","").replace("]]","").replace(".","")
        console.log(temp_raw_resp)
        raw_resp = Number(temp_raw_resp)
        console.log(raw_resp)
        this.mapped_result = this.raw_material_map[raw_resp];
        this.show_result = true;

      }
    }else{
      this.toast.error("An error occured")
    }
    this.spinner.hide()
  }

}
