import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(public http: HttpClient) { }


  public predict(payload)  : Promise<any>{
    return new Promise((resolve)=>{
      this.http.post(`${environment.base_url}/predict`,payload).subscribe(
        (resp: any) => {
          resolve(resp)
        },
        err => {
          console.log(err);
          resolve(err)
        }
      );
    })
  }

  public getQuestioneere() : Promise<any>{
    let jsonPayload = {
      "event": "get_questioneere",
      "prefix":"CUS-QTIONS",
    }

    return new Promise((resolve)=>{
      this.http.post(`${environment.scanner_api_lambda}`,jsonPayload).subscribe(
        (resp: any) => {
          resolve(resp)
        },
        err => {
          console.log(err);
          resolve(err)
        }
      );
    })

  }

  
}
