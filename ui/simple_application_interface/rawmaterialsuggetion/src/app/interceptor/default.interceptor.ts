import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';

import { Observable, of } from 'rxjs';
import 'rxjs/add/operator/do';

import { mergeMap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class DefaultInterceptor implements HttpInterceptor {

  constructor(public router: Router,
    public toastr: ToastrService){
}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // if(this.auth.getToken()){
    //     request = request.clone({
    //         setHeaders: {
    //             'Authorization': `${this.auth.getToken()}`
    //         }
    //     });
    // }
    //console.log(request)

    return next.handle(request).do((event: HttpEvent<any>) => {

        if(event instanceof HttpResponse){
           
        }

    },
    (err: any) => {
        if(err instanceof HttpErrorResponse){
            if (err.status === 401) {
               // this.auth.logoutHome();
                this.toastr.error('Token has expired.. Login Again');
            }
        }
    });

}
}
