export const environment = {
  production: true,
  base_url:"https://api.eha.eco/ml",
  //scanner_api_lambda :"https://dnkrgi5d0b.execute-api.ap-south-1.amazonaws.com/stag/scanner",
  scanner_api_lambda :"https://bwvksnpzec.execute-api.ap-south-1.amazonaws.com/prod/scanner",
  scanner_api_lambda_dev :"https://bwvksnpzec.execute-api.ap-south-1.amazonaws.com/prod/scanner"
};
