# Dependencies
from flask import Flask, request, redirect, url_for, flash, jsonify
import numpy as np
import pickle as p
import json
import joblib
import traceback
import pandas as pd
import os



# Your API definition
app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def predict():
    if model:
        try:
            #json_ = jsonify(request.json)
            data = request.get_json()

            print(data)

            prediction = np.array2string(model.predict(data))
            return jsonify(prediction)

        except:

            return jsonify({'trace': traceback.format_exc()})
    else:
        print ('Train the model first')
        return ('No model here to use')

if __name__ == '__main__':
    try:
        port = int(sys.argv[1]) # This is for a command-line input
    except:
        port = 3000 # If you don't provide any port the port will be set to 12345


    modelfile = os.path.abspath(os.getcwd())+'/Iteration 6/Notebook/model.pkl'
    model_columns = os.path.abspath(os.getcwd())+'/Iteration 6/Notebook/model_columns.pkl'
    model = joblib.load(open(modelfile, 'rb'))


    model_columns = joblib.load(model_columns) # Load "model_columns.pkl"
    print ('Model columns loaded')

    app.run(port=port, debug=True)